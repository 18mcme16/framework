// Provides smooth scroll behaviour to back to Top button

$(document).ready(function () {
        $('.ScrolltoTop').click(function () {
            // Removes scroll-behaviour style from CSS
            var htmltag = document.getElementsByTagName('html');
            htmltag[0].style.scrollBehavior = 'auto';
            $("html, body").animate({ scrollTop: 0 }, 1000);
            return false;
        });
});
