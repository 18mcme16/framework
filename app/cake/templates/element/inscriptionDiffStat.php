<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <th scope="col"><?= h('Artifact') ?></th>
            <th scope="col"><?= h('Revision') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($inscriptions as $update): ?>
            <tr align="left">
                <td><?= $this->Html->link(
                    h($update->artifact->designation . ' (' . $update->artifact->getCdliNumber() . ')'),
                    ['controller' => 'Artifacts', 'action' => 'view', $update->artifact->id]
                ) ?></td>
                <td><?= $this->Html->link(
                    h($update->id),
                    ['controller' => 'Inscriptions', 'action' => 'view', $update->id]
                ) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
