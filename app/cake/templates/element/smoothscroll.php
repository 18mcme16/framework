<!-- Provides smooth scroll behaviour to back to Top button -->

<?= $this->Html->script('smoothScroll.js')?>
<button type="button" class="ScrolltoTop p-0 mt-5 btn d-flex align-items-center justify-content-between mx-auto wide-btn">
<a href="#" class="text-dark btn wide-btn">Back to top<span class="fa fa-long-arrow-up ml-2" aria-hidden="true"></span>
</a>
</button>