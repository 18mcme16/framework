<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty $dynasty
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($dynasty->dynasty) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Polity') ?></th>
                    <td><?= h($dynasty->polity) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Dynasty') ?></th>
                    <td><?= h($dynasty->dynasty) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Provenience') ?></th>
                    <td><?= $dynasty->has('provenience') ? $this->Html->link($dynasty->provenience->id, ['controller' => 'Proveniences', 'action' => 'view', $dynasty->provenience->id]) : '' ?></td>
                </tr>
                <!-- <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($dynasty->id) ?></td>
                </tr> -->
                <tr>
                    <th scope="row"><?= __('Sequence') ?></th>
                    <td><?= $this->Number->format($dynasty->sequence) ?></td>
                </tr>
            </tbody>
        </table>

        <div class=" row float-right">

            <?php if($access_granted){ ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $dynasty->id],
                        ['escape' => false , 'class' => "btn btn-warning", 'title' => 'Edit']) ?>
            <?php } ?>
    
        </div>


    </div>

</div>


<div class="boxed mx-0">
    <?php  if (!empty($dynasty->dates)): ?>
        <div class="capital-heading"><?= __('Related Dates') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
<!--                 <th scope="col"><?= __('Id') ?></th>
 -->                <th scope="col"><?= __('Day No') ?></th>
                <th scope="col"><?= __('Day Remarks') ?></th>
                <th scope="col"><?= __('Month Id') ?></th>
                <th scope="col"><?= __('Is Uncertain') ?></th>
                <th scope="col"><?= __('Month No') ?></th>
                <th scope="col"><?= __('Year Id') ?></th>
                <th scope="col"><?= __('Dynasty Id') ?></th>
                <th scope="col"><?= __('Ruler Id') ?></th>
                <th scope="col"><?= __('Absolute Year') ?></th>
<!--                 <th scope="col"><?= __('Actions') ?></th>
 -->            </thead>
            <tbody>
                <?php foreach ($dynasty->dates as $dates): ?>
                <tr>
<!--                     <td><?= h($dates->id) ?></td>
 -->                    <td><?= h($dates->day_no) ?></td>
                    <td><?= h($dates->day_remarks) ?></td>
                    <td><?= h($dates->month_id) ?></td>
                    <td><?= h($dates->is_uncertain) ?></td>
                    <td><?= h($dates->month_no) ?></td>
                    <td><?= h($dates->year_id) ?></td>
                    <td><?= h($dates->dynasty_id) ?></td>
                    <td><?= h($dates->ruler_id) ?></td>
                    <td><?= h($dates->absolute_year) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Dates', 'action' => 'view', $dates->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Dates', 'action' => 'edit', $dates->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Dates', 'action' => 'delete', $dates->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $dates->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (!empty($dynasty->rulers)): ?>
        <div class="capital-heading"><?= __('Related Rulers') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
<!--                 <th scope="col"><?= __('Id') ?></th>
 -->                <th scope="col"><?= __('Sequence') ?></th>
                <th scope="col"><?= __('Ruler') ?></th>
                <th scope="col"><?= __('Period Id') ?></th>
                <th scope="col"><?= __('Dynasty Id') ?></th>
<!--                 <th scope="col"><?= __('Actions') ?></th>
 -->            </thead>
            <tbody>
                <?php foreach ($dynasty->rulers as $rulers): ?>
                <tr>
<!--                     <td><?= h($rulers->id) ?></td>
 -->                    <td><?= h($rulers->sequence) ?></td>
                    <td><?= h($rulers->ruler) ?></td>
                    <td><?= h($rulers->period_id) ?></td>
                    <td><?= h($rulers->dynasty_id) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Rulers', 'action' => 'view', $rulers->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Rulers', 'action' => 'edit', $rulers->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Rulers', 'action' => 'delete', $rulers->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $rulers->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


