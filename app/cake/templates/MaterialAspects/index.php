<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect[]|\Cake\Collection\CollectionInterface $materialAspects
 */
?>

<h3 class="display-4 pt-3"><?= __('Material Aspects') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('material_aspect') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materialAspects as $materialAspect): ?>
        <tr align="left">
            <td><a href="/materialAspects/<?=h($materialAspect->id)?>"><?= h($materialAspect->material_aspect) ?></a></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $materialAspect->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $materialAspect->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $materialAspect->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

