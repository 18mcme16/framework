<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience[]|\Cake\Collection\CollectionInterface $proveniences
 */
?>

<h3 class="display-4 pt-3"><?= __('Proveniences') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('provenience') ?></th>
            <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('geo_coordinates') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveniences as $provenience): ?>
        <tr align="left">
            <!-- <td><?= $this->Number->format($provenience->id) ?></td> -->
            <td><a href="/proveniences/<?=h($provenience->id)?>"><?= h($provenience->provenience) ?></a></td>
            <td><?= $provenience->has('region') ? $this->Html->link($provenience->region->region, ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]) : '' ?></td>
            <td><?= h($provenience->geo_coordinates) ?></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $provenience->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $provenience->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $provenience->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>