<?php
use Cake\I18n\FrozenTime;
/**
 * @var \App\View\AppView $this
 */
?>
<?php if ($access_granted): ?>
    <h3 class="display-4 pt-3"><?= __('Postings') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('posting_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('published') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lang') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publish_start') ?></th>
                <th scope="col"><?= $this->Paginator->sort('publish_end') ?></th>
                <th scope="col"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($postings as $posting): ?>
            <tr>
                <td><?= h($posting->posting_type->posting_type) ?></td>
                <td>
                    <?= 
                    $this->Html->link($posting->slug, ['controller' => 'postings', 'action' => 'view', $posting->id]);
                 ?></td>
                <td><?= h((FrozenTime::parse($posting->created))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
                <td><?= h((FrozenTime::parse($posting->modified))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
                <td><?= h($posting->published) ?></td>
                <td><?= h($posting->lang) ?></td>
                <td><?= h($posting->creator->author) ?></td>
                <td><?= h($posting->modifier->author) ?></td>
                <td><?= h($posting->publish_start) ?></td>
                <td><?= h($posting->publish_end) ?></td>
                <td>
                    <?= $this->Html->image("/images/edit.svg", [
                        "alt" => "Edit",
                        'url' => ['controller' => 'Postings', 'action' => 'edit', $posting->id],
                        'title' => 'Edit'
                    ]);
                    ?>
                    <!-- <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['action' => 'delete', $posting->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $posting->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?> -->
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php echo $this->element('Paginator'); ?>
<?php endif ?>
