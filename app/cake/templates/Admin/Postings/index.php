<?php
use Cake\I18n\FrozenTime;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting[]|\Cake\Collection\CollectionInterface $postings
 */
?>

<h3 class="display-4 pt-3"><?= __('Postings') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('posting_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col"><?= $this->Paginator->sort('published') ?></th>
            <th scope="col"><?= $this->Paginator->sort('lang') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_start') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publish_end') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($postings as $posting): ?>
        <tr>
            <!-- <td><?= $this->Number->format($posting->id) ?></td> -->
            <td><?= $posting->has('posting_type') ? $posting->posting_type->posting_type : '' ?></td>
            <td><?= $this->Html->link($posting->slug, ['controller' => '../postings', 'action' => 'view', $posting->id]);?></td>
            <td><?= h((FrozenTime::parse($posting->created))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
            <td><?= h((FrozenTime::parse($posting->modified))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
            <td><?= h($posting->published) ?></td>
            <td><?= h($posting->lang) ?></td>
            <td><?= h($posting->creator->author) ?></td>
            <td><?= h($posting->modifier->author) ?></td>
            <td><?= is_null($posting->publish_start)? h('') : h((FrozenTime::parse($posting->publish_start))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
            <td><?= is_null($posting->publish_end)? h('') : h((FrozenTime::parse($posting->publish_end))->i18nFormat('yyyy-MM-dd hh:mm a')) ?></td>
            <td>
                    <?= $this->Html->image("/images/edit.svg", [
                        "alt" => "Edit",
                        'url' => ['controller' => 'Postings', 'action' => 'edit', $posting->id],
                        'title' => 'Edit'
                    ]);
                    ?>
                </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
