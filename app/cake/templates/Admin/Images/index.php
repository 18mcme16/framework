<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Image[]|\Cake\Collection\CollectionInterface $images
 */
?>
<div class="images index content">
    <?= $this->Html->link(__('New Image'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Images') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Artifact ID') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('File Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('PPI') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Height') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Width') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Type of Visual Asset') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Folder Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Size (MB)') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Size (Mega Pixels)') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Creation Date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Modification Date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('RGB') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Format') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Bit') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Public') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($images as $image): ?>
                <tr>
                    <td><?= $image->has('artifact') ? $this->Html->link($image->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $image->artifact->id]) : '' ?></td>
                    <td><?= h($image->file_name) ?></td>
                    <td><?= $this->Number->format($image->ppi) ?></td>
                    <td><?= $this->Number->format($image->height) ?></td>
                    <td><?= $this->Number->format($image->width) ?></td>
                    <td><?= h($image->image_type) ?></td>
                    <td><?= h($image->folder_name) ?></td>
                    <td><?= $this->Number->format(str_split(($image->size_mb),4)[0]) ?></td>
                    <td><?= $this->Number->format(str_split(($image->size_pixels)/1000000,4)[0]) ?></td>
                    <td><?= h((str_split(($image->creation_date),16))[0]) ?></td>
                    <td><?= h((str_split(($image->modify_date),16))[0]) ?></td>
                    <td><?= h($image->rgb) ?></td>
                    <td><?= h($image->format) ?></td>
                    <td><?= $this->Number->format($image->bit) ?></td>
                    <td><?= h($image->is_public) ==0 ? "No" : "Yes" ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $image->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $image->id]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
