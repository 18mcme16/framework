<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Image $image
 */
$CDLI_NO = $image->artifact->getCdliNumber();
?>
<main id="artifact">
<a id="back" href="#" onclick="history.back()"><i class="fa fa-chevron-left"></i> Back to Search Results</a>
    <h1 class="display-3 header-txt"><?= h($image->artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
    <h2 class="my-4 artifact-desc">
        <?php if (!empty($image->artifact->genres)): ?>
            <?= h($image->artifact->genres[0]->genre) ?>,
        <?php endif; ?>
        <?php if (!empty($image->artifact->artifact_type)): ?>
            <?= h($image->artifact->artifact_type->artifact_type) ?>,
        <?php endif; ?>
        <?php if (!empty($image->artifact->provenience)): ?>
            <?= h($image->artifact->provenience->provenience) ?>
        <?php endif; ?>
        <?php if (!empty($image->artifact->period)): ?>
            in <?= h($image->artifact->period->period) ?>
        <?php endif; ?>
        <?php if (!empty($image->artifact->collections)): ?>
            and kept at <?= h($image->artifact->collections[0]->collection) ?>
        <?php endif; ?>
    </h2>
    <div>
        <div class="artifact-summary font-weight-light">
            <h2 class="font-weight-light">Summary of the artifact</h2>
            <div class="mt-4 d-block d-md-flex justify-content-between">
                <div>
                    <p>Musuem Collections</p>
                    <?php foreach ($image->artifact->collections as $collection): ?>
                        <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                    <?php endforeach; ?>

                    <p>Period</p>
                    <?php if (!empty($image->artifact->period)): ?>
                        <?= $this->Html->link($image->artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $image->artifact->period->id]) ?>
                    <?php endif; ?>

                    <p>Provenience</p>
                    <?php if (!empty($image->artifact->provenience)): ?>
                        <?= $this->Html->link($image->artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $image->artifact->provenience->id]) ?>
                    <?php endif; ?>

                    <p>Artifact Type</p>
                    <?php if (!empty($image->artifact->artifact_type)): ?>
                        <?= $this->Html->link($image->artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $image->artifact->artifact_type->id]) ?>
                    <?php endif; ?>
            </div>


            <div>
                <p>Material</p>
                <?php foreach ($image->artifact->materials as $material): ?>
                    <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                <?php endforeach; ?>

                <p>Genre / Subgenre</p>
                <?php foreach ($image->artifact->genres as $genre): ?>
                    <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
                <?php endforeach; ?>

                <p>Language</p>
                <?php foreach ($image->artifact->languages as $language): ?>
                    <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="pt-5 d-flex justify-content-center">
        <p>The below table will display all the <strong>Visual Assets</strong> related to this Artifact.</p>
    </div>
    <div class="pt-5">
        <table class="table table-hover table-bordered">
            <thead align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('Image') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Folder Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('File Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('PPI') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Height') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Width') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Type of Visual Asset') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Size (MB)') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Size (Mega Pixels)') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Creation Date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Modification Date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('RGB') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Format') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Bit') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Public') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($rows as $row): ?>
                    <?php if ($row['image_type']=='rti_observe' or $row['image_type']=='3D_model_e' or $row['image_type']=='lineart' or $row['image_type']=='lineart_detail' or $row['image_type']=='pdf' or $row['image_type']=='photo_envelope' or $row['image_type']=='photo_detail' or $row['image_type']=='ptm' or $row['image_type']=='svg' or $row['image_type']=='thumb_lineart' or $row['image_type']=='thumb_photo'or $row['image_type']=='3D_model' or $row['image_type']=='3D_model_a' or $row['image_type']=='3D_model_b' or $row['image_type']=='photo'):?>
                        <tr align="left">
                            <td align="left" nowrap="nowrap">
                                <?php if($row['format']=='.pdf'):?>
                                    <a href="/dl<?=h($row['folder_name'])?>/<?=h($row['file_name'])?>" target="_blank"><?= h($row['file_name']) ?></a>
                                <?php else: ?>
                                    <div>
                                        <img src="/dl<?=h($row['folder_name'])?>/<?=h($row['file_name'])?>" width="150" height="150">
                                        <div class="pt-2 d-flex justify-content-center">
                                            <a href="/dl<?=h($row['folder_name'])?>/<?=h($row['file_name'])?>" class="card-title" target="_blank">View Full Image</a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['folder_name'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['file_name'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['ppi'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['height'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['width'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['image_type'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h(str_split(($row['size_mb']),4)[0])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h(str_split(($row['size_pixels'])/1000000,4)[0])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h((str_split(($row['creation_date']),16))[0])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h((str_split(($row['modify_date']),16))[0])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['rgb'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['format'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['bit'])?></p>
                            </td>
                            <td align="left" nowrap="nowrap">
                                <p><?= h($row['is_public']==0 ? "No": "Yes")?></p>
                            </td>
                            <td align="left" nowrap="nowrap" class="actions">
                                <?= $this->Html->link(__('Edit'),['controller'=>'Images', 'action'=>'edit', $row['id']],['class'=>"btn btn-warning btn-sm"]) ?>
                            </td>
                        </tr>
                    <?php endif ;?>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</main>
<?= $this->Html->script('focus-visible.js') ?>
<?php echo $this->element('smoothscroll'); ?>