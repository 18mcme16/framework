<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upload[]|\Cake\Collection\CollectionInterface $uploads
 */
?>
<style>


</style>
<div class="container">
    <?php if(array_key_exists(1,$uploads)){ ?>
        <div class="alert alert-warning alert-dismissible fade show m-0" role="alert">
        <p class="m-0"><strong><?= count($uploads['1']) ?></strong> files were uploaded.</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    <?php } ?>
    <?php if(array_key_exists(0,$uploads)){ ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <p class="m-0"><strong><?= count($uploads['0']) ?></strong> files failed while uploading.<br>
        <?= implode(', ', $uploads['0'])?></p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    <?php } ?>

    <div class="container horizontal-scrollable" id="raw">

        <div class="row text-cente">
            <div class="col m-2">
                <p class="h2">Raw Archivals</p>
            </div>
        </div>

        <div class="row text-center">
            <?php
            if($unprocessed){
            echo $this->Form->create(null, ['class' => 'col']);
            echo "<div class='row'>";
            foreach($unprocessed as $pno => $count) {
                $pno = $pno ? $pno : "Invalid";?>
                <div class="col m-3">
                    <div class="wrapper" id=<?= $pno?>>
                        <input type="checkbox" class="form-check-input move-left" name="<?= $pno?>" checked value="true">
                        <?php
                        if ($count == 6) {
                            echo '<svg width="4rem" height="4rem" viewBox="0 0 16 16" class="bi bi-file-earmark-check-fill" fill="lightgreen" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 3a2 2 0 0 1 2-2h5.293a1 1 0 0 1 .707.293L13.707 5a1 1 0 0 1 .293.707V13a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3zm7 2V2l4 4h-3a1 1 0 0 1-1-1zm1.854 2.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
                            </svg>';
                            echo "<p class='4 m-0'><strong>{$pno}</strong></p>";
                        }
                        else{
                            echo '<svg width="4rem" height="4rem" viewBox="0 0 16 16" class="bi bi-file-earmark-minus-fill" fill="tomato" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 3a2 2 0 0 1 2-2h5.293a1 1 0 0 1 .707.293L13.707 5a1 1 0 0 1 .293.707V13a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3zm7 2V2l4 4h-3a1 1 0 0 1-1-1zM6 8.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1H6z"/>
                            </svg>';
                            echo "<p class='4 m-0 strong'><strong>{$pno} ({$count})</strong></p>";
                        }
                        ?>
                    </div>
                </div>
            <?php }
                echo "</div>";
            ?>
            <div class="row align-items-end">
                <div class="col m-2">
                    <button type="submit" class="btn btn-success float-right">Create Task</button>
                </div>
            </div>
            <?php echo $this->Form->end();}?>
        </div>

        <div class="row text-center p-2">
            <div class="col">
                <p class="h2">Processed Archivals</p>
            </div>
        </div>
        <div class="row text-center">
        <?php
            if($processed){
            echo "<div class='row'>";
            foreach($processed as $pno) {
                $pno = $pno ? $pno : "Invalid";?>
                <div class="col m-3">
                    <a href="/admin/uploads/edit/<?= $pno ?>">
                        <?php
                            echo '<svg width="4em" height="4em" viewBox="0 0 16 16" class="bi bi-file-earmark-zip-fill" fill="lightblue" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 3a2 2 0 0 1 2-2h.5v1h1v1h-1v1h1v1h-1v1h1v1H7V6H6V5h1V4H6V3h1V2H6V1h3.293a1 1 0 0 1 .707.293L13.707 5a1 1 0 0 1 .293.707V13a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3zm7 2V2l4 4h-3a1 1 0 0 1-1-1zM5.5 7.5a1 1 0 0 0-1 1v.938l-.4 1.599a1 1 0 0 0 .416 1.074l.93.62a1 1 0 0 0 1.109 0l.93-.62a1 1 0 0 0 .415-1.074l-.4-1.599V8.5a1 1 0 0 0-1-1h-1zm0 1.938V8.5h1v.938a1 1 0 0 0 .03.243l.4 1.598-.93.62-.93-.62.4-1.598a1 1 0 0 0 .03-.243z"/>
                        </svg>';
                            echo "<p class='4 m-0'><strong>{$pno}</strong></p>";
                        ?>
                    </a>
                </div>
            <?php }
                echo "</div>";
            }?>
        </div>
        <div class="row text-center p-2">
            <div class="col">
                <p class="h2">Completed Archivals</p>
            </div>
        </div>
        <div class="row text-center">
        <?php
            if($completed){
            echo "<div class='row'>";
            foreach($completed as $pno) {
                $pno = $pno ? $pno : "Invalid";?>
                <div class="col m-3">
                    <a href="/admin/uploads/<?= $pno ?>">
                        <?php
                            echo '<svg width="3.5rem" height="3.5rem" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="lightgreen" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                          </svg>';
                            echo "<p class='4 m-0'><strong>{$pno}</strong></p>";
                        ?>
                    </a>
                </div>
            <?php }
                echo "</div>";
            }?>
        </div>
    </div>
</div>
