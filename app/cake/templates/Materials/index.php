<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material[]|\Cake\Collection\CollectionInterface $materials
 */
?>


<h3 class="display-4 pt-3"><?= __('Materials') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
<!--             <th scope="col"><?= $this->Paginator->sort('id') ?></th>
 -->            <th scope="col"><?= $this->Paginator->sort('material') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material): ?>
        <tr align="left">
<!--             <td><?= $this->Number->format($material->id) ?></td>
 -->            <td><a href="/materials/<?=h($material->id)?>"><?= h($material->material) ?></a></td>
            <td><?= $material->has('parent_material') ? $this->Html->link($material->parent_material->material, ['controller' => 'Materials', 'action' => 'view', $material->parent_material->id]) : '' ?></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $material->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $material->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $material->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

