<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent $event
 */

$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="abbreviations form content">
            <?= $this->Form->create($event) ?>
                <fieldset>
                    <legend><?= __('Edit update event') ?></legend>
                    <?= $this->Form->control('event_comments', [
                        'class' => 'form-control'
                    ]); ?>
                    <?= $this->Form->control('external_resource_id', [
                        'class' => 'form-control',
                        'options' => $external_resources,
                        'empty' => true
                    ]) ?>
                </fieldset>
                <div>
                    <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Html->link(
                        __('Cancel'),
                        ['action' => 'view', $event->id],
                        ['class' => 'btn']
                    ) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $event->id], [
                        'class' => 'btn btn-danger float-right',
                        'confirm' => __('Are you sure you want to delete # {0}?', $event->id)
                    ]) ?>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

</div>
