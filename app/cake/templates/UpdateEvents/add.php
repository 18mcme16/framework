<?php
/**
 * @var \App\View\AppView $this
 */
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($event) ?>
            <legend class="capital-heading"><?= __('Update') ?></legend>
            <?= $this->Form->control('update_type', [
                'class' => 'form-control',
                'disabled' => true,
                'options' => [
                    ['text' => 'Transliteration', 'value' => 'atf'],
                    ['text' => 'Translation', 'value' => 'translation'],
                    ['text' => 'Annotation', 'value' => 'annotation'],
                    ['text' => 'Metadata', 'value' => 'artifact', 'selected' => true]
                ],
                'val' => $event->update_type
            ]) ?>
            <?= $this->Form->control('event_comments', [
                'class' => 'form-control'
            ]) ?>
            <?= $this->Form->control('external_resource_id', [
                'class' => 'form-control',
                'options' => $external_resources,
                'empty' => true
            ]) ?>

            <div class="submit">
                <?php if ($canReviewEdits): ?>
                    <?= $this->Form->button(__('Save and apply'), [
                        'class' => 'btn cdli-btn-blue',
                        'name' => 'action',
                        'value' => 'approved'
                    ]) ?>
                    <?= $this->Form->button(__('Save and submit'), [
                        'class' => 'btn cdli-btn-light',
                        'name' => 'action',
                        'value' => 'submitted'
                    ]) ?>
                    <?= $this->Form->button(__('Save'), [
                        'class' => 'btn cdli-btn-light',
                        'name' => 'action',
                        'value' => 'created'
                    ]) ?>
                <?php else: ?>
                    <?= $this->Form->button(__('Save'), [
                        'class' => 'btn cdli-btn-blue',
                        'name' => 'action',
                        'value' => 'created'
                    ]) ?>
                    <?= $this->Form->button(__('Save and submit'), [
                        'class' => 'btn cdli-btn-light',
                        'name' => 'action',
                        'value' => 'submitted'
                    ]) ?>
                <?php endif; ?>
                <?= $this->Html->link(__('Cancel'), ['action' => 'cancel'], [
                    'class' => 'btn cdli-btn-light',
                    'value' => 'created'
                ]) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php if ($event->update_type == 'artifact' && !empty($event->artifacts_updates)) {
            echo $this->element('artifactUpdateDiffStat', ['artifacts_updates' => $event->artifacts_updates]);
        } else if (!empty($event->inscriptions)) {
            echo $this->element('inscriptionDiffStat', ['inscriptions' => $event->inscriptions]);
        }?>
    </div>
</div>
