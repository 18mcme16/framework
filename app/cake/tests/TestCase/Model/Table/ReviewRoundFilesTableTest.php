<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReviewRoundFilesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReviewRoundFilesTable Test Case
 */
class ReviewRoundFilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReviewRoundFilesTable
     */
    protected $ReviewRoundFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ReviewRoundFiles',
        'app.Submissions',
        'app.ReviewRounds',
        'app.Stages',
        'app.Files',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ReviewRoundFiles') ? [] : ['className' => ReviewRoundFilesTable::class];
        $this->ReviewRoundFiles = $this->getTableLocator()->get('ReviewRoundFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ReviewRoundFiles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundFilesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundFilesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     * @uses \App\Model\Table\ReviewRoundFilesTable::defaultConnectionName()
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
