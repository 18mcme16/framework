<?php

namespace App\Utility;

use SebastianBergmann\Diff\Differ;
use SebastianBergmann\Diff\Output\DiffOutputBuilderInterface;

class HtmlDiffOutputBuilder implements DiffOutputBuilderInterface
{
    public function getDiff(array $diff): string
    {
        $buffer = fopen('php://memory', 'r+b');

        foreach ($diff as $diffEntry) {
            if ($diffEntry[1] === Differ::ADDED) {
                fwrite($buffer, '<ins>' . $diffEntry[0] . '</ins>');
            } elseif ($diffEntry[1] === Differ::REMOVED) {
                fwrite($buffer, '<del>' . $diffEntry[0] . '</del>');
            } elseif ($diffEntry[1] === Differ::DIFF_LINE_END_WARNING) {
                fwrite($buffer, $diffEntry[0] . '<span class="fas fa-exclamation-circle" title="Different line ends"></span>');

                continue; // Warnings should not be tested for line break, it will always be there
            } else { /* Not changed (old) 0 */
                fwrite($buffer, $diffEntry[0]);
            }

            $lc = substr($diffEntry[0], -1);

            if ($lc !== "\n" && $lc !== "\r") {
                fwrite($buffer, "\n"); // \No newline at end of file
            }
        }

        $diff = stream_get_contents($buffer, -1, 0);
        fclose($buffer);

        return $diff;
    }
}
