<?php
namespace App\Error;

use Cake\Core\Configure;
use Cake\Error\ExceptionRenderer;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;

class AppExceptionRenderer extends ExceptionRenderer
{
    /**
     * @return \Cake\Http\Response
     */
    public function render(): \Cake\Http\Response
    {
        if ($this->error instanceof UnauthorizedException ||
            $this->error instanceof ForbiddenException) {
            $this->error = new NotFoundException();
        }

        $type = $this->controller->RequestHandler->prefers();
        if ($type !== 'html' && !Configure::read('debug')) {
            $code = $this->error->getCode();
            return $this->controller->response->withStatus($code);
        }

        return parent::render();
    }
}
