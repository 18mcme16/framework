<?php
/* src/View/Helper/ArtifactImagesHelper.php */
namespace App\View\Helper;

use Cake\View\Helper;

class ArtifactImagesHelper extends Helper
{
    public function getMainImage($artifact_id)
    {
        if ($artifact_id == '') {
            $main_image['thumbnail'] = $main_image['main_image'] = 'https://mcdn.wallpapersafari.com/medium/22/59/v2u6ZO.jpg';
            $main_image['image_type']= 'No Image';
            return $main_image;
        } else {
            $Pno = 'P'.str_pad($artifact_id, 6, "0", STR_PAD_LEFT);
            $full_path = '/srv/app/cake/webroot';
            $photo = '/dl/tn_photo/'.$Pno.'.jpg';
            $photo_d = '/dl/tn_photo/'.$Pno.'_d.jpg';
            $lineart = '/dl/tn_lineart/'.$Pno.'.jpg';
            $lineart_d = '/dl/tn_lineart/'.$Pno.'_d.jpg';

            if (file_exists($full_path.$photo)) {
                $main_image['thumbnail'] =  $photo;
                $main_image['main_image'] = '/dl/photo/'.$Pno.'.jpg';
                $main_image['image_type']= 'Photo';
                return $main_image;
            } elseif (file_exists($full_path.$photo_d)) {
                $main_image['thumbnail'] =  $photo_d;
                $main_image['main_image'] = '/dl/photo/'.$Pno.'_d.jpg';
                $main_image['image_type']= 'Photo Detail';
                return $main_image;
            } elseif (file_exists($full_path.$lineart)) {
                $main_image['thumbnail'] =  lineart;
                $main_image['main_image'] = '/dl/lineart/'.$Pno.'.jpg';
                $main_image['image_type']= 'Line Art';
                return $main_image;
            } elseif (file_exists($full_path.$lineart_d)) {
                $main_image['thumbnail'] =  $lineart_d;
                $main_image['main_image'] = '/dl/lineart/'.$Pno.'_d.jpg';
                $main_image['image_type']= 'Detail Line Art';
                return $main_image;
            } else {
                $main_image['thumbnail'] = $main_image['main_image'] = 'https://mcdn.wallpapersafari.com/medium/22/59/v2u6ZO.jpg';
                $main_image['image_type']= 'No Image';
                return $main_image;
            }
        }
    }
}
