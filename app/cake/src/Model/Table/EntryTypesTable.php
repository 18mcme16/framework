<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EntryTypes Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\HasMany $Publications
 *
 * @method \App\Model\Entity\EntryType newEmptyEntity()
 * @method \App\Model\Entity\EntryType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EntryType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntryType get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntryType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EntryType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntryType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntryType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntryType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntryType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntryType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntryType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntryType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EntryTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('entry_types');
        $this->setDisplayField('label');
        $this->setPrimaryKey('id');

        $this->hasMany('Publications', [
            'foreignKey' => 'entry_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('label')
            ->maxLength('label', 25)
            ->allowEmptyString('label');

        $validator
            ->scalar('author')
            ->maxLength('author', 3)
            ->allowEmptyString('author');

        $validator
            ->scalar('title')
            ->maxLength('title', 3)
            ->allowEmptyString('title');

        $validator
            ->scalar('journal')
            ->maxLength('journal', 3)
            ->allowEmptyString('journal');

        $validator
            ->scalar('year')
            ->maxLength('year', 3)
            ->allowEmptyString('year');

        $validator
            ->scalar('volume')
            ->maxLength('volume', 3)
            ->allowEmptyString('volume');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 3)
            ->allowEmptyString('pages');

        $validator
            ->scalar('number')
            ->maxLength('number', 3)
            ->allowEmptyString('number');

        $validator
            ->scalar('month')
            ->maxLength('month', 3)
            ->allowEmptyString('month');

        $validator
            ->scalar('eid')
            ->maxLength('eid', 3)
            ->allowEmptyString('eid');

        $validator
            ->scalar('note')
            ->maxLength('note', 3)
            ->allowEmptyString('note');

        $validator
            ->scalar('crossref')
            ->maxLength('crossref', 3)
            ->allowEmptyString('crossref');

        $validator
            ->scalar('keyword')
            ->maxLength('keyword', 3)
            ->allowEmptyString('keyword');

        $validator
            ->scalar('doi')
            ->maxLength('doi', 3)
            ->allowEmptyString('doi');

        $validator
            ->scalar('url')
            ->maxLength('url', 3)
            ->allowEmptyString('url');

        $validator
            ->scalar('file')
            ->maxLength('file', 3)
            ->allowEmptyFile('file');

        $validator
            ->scalar('citeseerurl')
            ->maxLength('citeseerurl', 3)
            ->allowEmptyString('citeseerurl');

        $validator
            ->scalar('pdf')
            ->maxLength('pdf', 3)
            ->allowEmptyString('pdf');

        $validator
            ->scalar('abstract')
            ->maxLength('abstract', 3)
            ->allowEmptyString('abstract');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 3)
            ->allowEmptyString('comment');

        $validator
            ->scalar('owner')
            ->maxLength('owner', 3)
            ->allowEmptyString('owner');

        $validator
            ->scalar('timestamp')
            ->maxLength('timestamp', 3)
            ->allowEmptyString('timestamp');

        $validator
            ->scalar('review')
            ->maxLength('review', 3)
            ->allowEmptyString('review');

        $validator
            ->scalar('search')
            ->maxLength('search', 3)
            ->allowEmptyString('search');

        $validator
            ->scalar('publisher')
            ->maxLength('publisher', 3)
            ->allowEmptyString('publisher');

        $validator
            ->scalar('editor')
            ->maxLength('editor', 3)
            ->allowEmptyString('editor');

        $validator
            ->scalar('series')
            ->maxLength('series', 3)
            ->allowEmptyString('series');

        $validator
            ->scalar('address')
            ->maxLength('address', 3)
            ->allowEmptyString('address');

        $validator
            ->scalar('edition')
            ->maxLength('edition', 3)
            ->allowEmptyString('edition');

        $validator
            ->scalar('howpublished')
            ->maxLength('howpublished', 3)
            ->allowEmptyString('howpublished');

        $validator
            ->scalar('lastchecked')
            ->maxLength('lastchecked', 3)
            ->allowEmptyString('lastchecked');

        $validator
            ->scalar('booktitle')
            ->maxLength('booktitle', 3)
            ->allowEmptyString('booktitle');

        $validator
            ->scalar('organization')
            ->maxLength('organization', 3)
            ->allowEmptyString('organization');

        $validator
            ->scalar('language')
            ->maxLength('language', 3)
            ->allowEmptyString('language');

        $validator
            ->scalar('chapter')
            ->maxLength('chapter', 3)
            ->allowEmptyString('chapter');

        $validator
            ->scalar('type')
            ->maxLength('type', 3)
            ->allowEmptyString('type');

        $validator
            ->scalar('school')
            ->maxLength('school', 3)
            ->allowEmptyString('school');

        $validator
            ->scalar('nationality')
            ->maxLength('nationality', 3)
            ->allowEmptyString('nationality');

        $validator
            ->scalar('yearfiled')
            ->maxLength('yearfiled', 3)
            ->allowEmptyFile('yearfiled');

        $validator
            ->scalar('assignee')
            ->maxLength('assignee', 3)
            ->allowEmptyString('assignee');

        $validator
            ->scalar('day')
            ->maxLength('day', 3)
            ->allowEmptyString('day');

        $validator
            ->scalar('dayfiled')
            ->maxLength('dayfiled', 3)
            ->allowEmptyFile('dayfiled');

        $validator
            ->scalar('monthfiled')
            ->maxLength('monthfiled', 3)
            ->allowEmptyFile('monthfiled');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 3)
            ->allowEmptyString('institution');

        $validator
            ->scalar('revision')
            ->maxLength('revision', 3)
            ->allowEmptyString('revision');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }
}
