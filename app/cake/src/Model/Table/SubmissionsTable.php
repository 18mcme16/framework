<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Submissions Model
 *
 * @property \App\Model\Table\SectionsTable&\Cake\ORM\Association\BelongsTo $Sections
 * @property \App\Model\Table\PublicationsOjsTable&\Cake\ORM\Association\BelongsTo $PublicationsOjs
 * @property \App\Model\Table\ReviewAssignmentsTable&\Cake\ORM\Association\HasMany $ReviewAssignments
 * @property \App\Model\Table\QueriesTable&\Cake\ORM\Association\HasMany $Queries
 * @property \App\Model\Table\PublicationSettingsTable&\Cake\ORM\Association\HasMany $PublicationSettings
 *
 * @method \App\Model\Entity\Submission newEmptyEntity()
 * @method \App\Model\Entity\Submission newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Submission[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Submission get($primaryKey, $options = [])
 * @method \App\Model\Entity\Submission findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Submission patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Submission[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Submission|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Submission saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Submission[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Submission[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Submission[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Submission[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SubmissionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('submissions');
        $this->setDisplayField('submission_id');
        $this->setPrimaryKey('submission_id');
        $this->belongsTo('Sections', [
            'foreignKey' => 'section_id',
        ]);
        $this->hasMany('ReviewAssignments');
        $this->hasMany('Queries', [
            'foreignKey' => 'assoc_id',
            'bindingKey' => 'submission_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PublicationSettings', [
            'foreignKey' => 'publication_id',
            'bindingKey' => 'current_publication_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PublicationsOjs');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('submission_id', null, 'create');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->allowEmptyString('locale');

        $validator
            ->dateTime('date_last_activity')
            ->allowEmptyDateTime('date_last_activity');

        $validator
            ->dateTime('date_submitted')
            ->allowEmptyDateTime('date_submitted');

        $validator
            ->dateTime('last_modified')
            ->allowEmptyDateTime('last_modified');

        $validator
            ->notEmptyString('status');

        $validator
            ->notEmptyString('submission_progress');

        $validator
            ->allowEmptyString('work_type');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
