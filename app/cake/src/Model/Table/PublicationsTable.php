<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Publications Model
 *
 * @property \App\Model\Table\EntryTypesTable&\Cake\ORM\Association\BelongsTo $EntryTypes
 * @property \App\Model\Table\JournalsTable&\Cake\ORM\Association\BelongsTo $Journals
 * @property \App\Model\Table\AbbreviationsTable&\Cake\ORM\Association\HasMany $Abbreviations
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsToMany $Articles
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Artifacts
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Editors
 *
 * @method \App\Model\Entity\Publication newEmptyEntity()
 * @method \App\Model\Entity\Publication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Publication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Publication get($primaryKey, $options = [])
 * @method \App\Model\Entity\Publication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Publication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Publication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Publication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class PublicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('publications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('EntryTypes', [
            'foreignKey' => 'entry_type_id'
        ]);
        $this->belongsTo('Journals', [
            'foreignKey' => 'journal_id'
        ]);
        $this->hasMany('Abbreviations', [
            'foreignKey' => 'publication_id'
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'articles_publications'
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_publications'
        ]);
        $this->belongsToMany('Authors', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_publications',
            'propertyName' => 'authors'
        ]);
        $this->belongsToMany('Editors', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'editor_id',
            'joinTable' => 'editors_publications',
            'through' => 'EditorsPublications',
            'propertyName' => 'editors'
        ]);
    }

    /**
     * Convert input data to required format.
     */
    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        // Removing leading and trailing whitespaces
        foreach ($data as $key => $value) {
            if ($key != 'artifacts') {
                $data[$key] = (!empty(trim($value))) ? trim($value):null;
            }
        }

        // Mapping field values to their ids
        $publications = TableRegistry::getTableLocator()->get('Publications');
        $entryTypes = $publications->EntryTypes->find('list', [
            'keyField' => 'label',
            'valueField' => 'id'
            ])->toArray();
        if ((isset($data['entry_type_id'])) and (!in_array($data['entry_type_id'], array_values($entryTypes)))) {
            $data['entry_type_id'] = (isset($entryTypes[$data['entry_type_id']])) ? $entryTypes[$data['entry_type_id']]:0;
        }
        
        $journals = $publications->Journals->find('list', [
            'keyField' => 'journal',
            'valueField' => 'id'
            ])->toArray();
        if ((isset($data['journal_id'])) and (!in_array($data['journal_id'], array_values($journals)))) {
            $data['journal_id'] = (isset($journals[$data['journal_id']])) ? $journals[$data['journal_id']]:0;
        }
        
        // Conversion to format for AuthorsPublication entity
        if (isset($data['authors'])) {
            $author_list = explode(';', $data['authors']);
            $data['authors'] = [];
            $authors = TableRegistry::getTableLocator()->get('Authors');
            foreach ($author_list as $key => $authorname) {
                $author = $authors->find('all', ['conditions' => ['author' => trim($authorname)]])->first();
                $author_id = (isset($author)) ? $author->id:-1;
                array_push($data['authors'], ['id' => $author_id, '_joinData' => ['sequence' => $key]]);
            }
        }

        // Conversion to format for EditorsPublication entity
        if (isset($data['editors'])) {
            $editors = TableRegistry::getTableLocator()->get('Editors');
            $editor_list = explode(';', $data['editors']);
            $data['editors'] = [];
            foreach ($editor_list as $key => $editorname) {
                $editor = $editors->find('all', ['conditions' => ['author' => trim($editorname)]])->first();
                $editor_id = (isset($editor)) ? $editor->id:-1;
                array_push($data['editors'], ['id' => $editor_id, '_joinData' => ['sequence' => $key]]);
            }
        }

        // Generating bibtexkey if it is empty
        if ((!isset($data['bibtexkey'])) and (isset($data['authors']) and isset($data['year']))) {
            $author = $authors->get($data['authors'][0]['id']);
            $author_last_name = explode(',', $author->author)[0];
            $bibtexkey = Text::slug($author_last_name).$data['year'];

            // Ensure the bibtexkey is unique
            $similar_bibtexkeys = $this->find('all', [
                'fields' => ['bibtexkey'],
                'order' => ['bibtexkey' => 'asc']
                ])->where(['bibtexkey LIKE' => '%'. $bibtexkey.'%'])->toArray();
            $similar_bibtexkeys = array_column($similar_bibtexkeys, 'bibtexkey');
            if (in_array($bibtexkey, $similar_bibtexkeys)) {
                $suffix = 'a';
                while (in_array($bibtexkey.$suffix, $similar_bibtexkeys)) {
                    $suffix++;
                    debug($suffix);
                }
                $bibtexkey = $bibtexkey.$suffix;
            }
            $data['bibtexkey'] = $bibtexkey;
        }

        // Temporary default values
        $data['accepted'] = 1;
        $data['accepted_by'] = 35;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('bibtexkey')
            ->allowEmptyString('bibtexkey');

        $validator
            ->scalar('year')
            ->maxLength('year', 20)
            ->allowEmptyString('year');

        $validator
            ->scalar('address')
            ->maxLength('address', 45)
            ->allowEmptyString('address');

        $validator
            ->scalar('annote')
            ->maxLength('annote', 45)
            ->allowEmptyString('annote');

        $validator
            ->scalar('book_title')
            ->maxLength('book_title', 255)
            ->allowEmptyString('book_title');

        $validator
            ->scalar('chapter')
            ->maxLength('chapter', 100)
            ->allowEmptyString('chapter');

        $validator
            ->scalar('crossref')
            ->maxLength('crossref', 45)
            ->allowEmptyString('crossref');

        $validator
            ->scalar('edition')
            ->maxLength('edition', 45)
            ->allowEmptyString('edition');

        $validator
            ->scalar('editor')
            ->maxLength('editor', 100)
            ->allowEmptyString('editor');

        $validator
            ->scalar('how_published')
            ->maxLength('how_published', 255)
            ->allowEmptyString('how_published');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 45)
            ->allowEmptyString('institution');

        $validator
            ->scalar('month')
            ->maxLength('month', 45)
            ->allowEmptyString('month');

        $validator
            ->scalar('note')
            ->maxLength('note', 45)
            ->allowEmptyString('note');

        $validator
            ->scalar('number')
            ->maxLength('number', 100)
            ->allowEmptyString('number');

        $validator
            ->scalar('organization')
            ->maxLength('organization', 45)
            ->allowEmptyString('organization');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 45)
            ->allowEmptyString('pages');

        $validator
            ->scalar('publisher')
            ->maxLength('publisher', 100)
            ->allowEmptyString('publisher');

        $validator
            ->scalar('school')
            ->maxLength('school', 80)
            ->allowEmptyString('school');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('volume')
            ->maxLength('volume', 50)
            ->allowEmptyString('volume');

        $validator
            ->scalar('publication_history')
            ->allowEmptyString('publication_history');

        $validator
            ->scalar('series')
            ->maxLength('series', 100)
            ->allowEmptyString('series');

        $validator
            ->integer('oclc')
            ->allowEmptyString('oclc');

        $validator
            ->scalar('designation')
            ->allowEmptyString('designation');

        $validator
            ->nonNegativeInteger('accepted_by')
            ->requirePresence('accepted_by', 'create')
            ->notEmptyString('accepted_by');

        $validator
            ->boolean('accepted')
            ->requirePresence('accepted', 'create')
            ->notEmptyString('accepted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->isUnique(['bibtexkey'], 'This Bibtex key already exists'));
        $rules->add($rules->isUnique(['designation'], 'This designation value already exists'));
        $rules->add($rules->existsIn(['entry_type_id'], 'EntryTypes'), ['errorField' => 'entry_type_id']);
        $rules->add($rules->existsIn(['journal_id'], 'Journals'), ['errorField' => 'journal_id']);

        return $rules;
    }

    public function afterSaveCommit($event, $entity, $options)
    {
        if (!isset($entity->bibtexkey)) {
            $entity->bibtexkey = $entity->id.'-'.(Text::slug($entity->title));
            $this->save($entity);
        }
    }
}
