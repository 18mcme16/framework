<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReviewRoundFile Entity
 *
 * @property int $submission_id
 * @property int $review_round_id
 * @property int $stage_id
 * @property int $file_id
 * @property int $revision
 *
 * @property \App\Model\Entity\Submission $submission
 * @property \App\Model\Entity\ReviewRound $review_round
 * @property \App\Model\Entity\Stage $stage
 * @property \App\Model\Entity\SubmissionFile $submission_file
 */
class ReviewRoundFile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'submission_id' => true,
        'review_round_id' => true,
        'stage_id' => true,
        'file_id' => true,
        'revision' => true,
        'submission' => true,
        'review_round' => true,
        'stage' => true,
        'submission_file' => true,
    ];
}
