<?php
namespace App\Model\Entity;

use App\Model\Entity\User;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * UpdateEvent Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $created_by
 * @property int|null $external_resource_id
 * @property string|null $update_type
 * @property string|null $event_comments
 * @property int|null $approved_by
 * @property string $status
 *
 * @property \App\Model\Entity\User $creator
 * @property \App\Model\Entity\User $reviewer
 * @property \App\Model\Entity\Author[] $authors
 */
class UpdateEvent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'created_by' => true,
        'external_resource_id' => true,
        'update_type' => true,
        'event_comments' => true,
        'approved_by' => true,
        'status' => true,
        'creator' => true,
        'reviewer' => true,
        'authors' => true,
    ];

    public function apply()
    {
        if ($this->update_type === 'artifact') {
            TableRegistry::get($this->getSource())->loadInto($this, ['ArtifactsUpdates.Artifacts']);
            $Artifacts = TableRegistry::get('Artifacts');
            $ArtifactsShadow = TableRegistry::get('ArtifactsShadow');
            $ArtifactsUpdates = TableRegistry::get('ArtifactsUpdates');

            foreach ($this->artifacts_updates as $artifacts_update) {
                $artifact = $artifacts_update->artifact;
                if (empty($artifact)) {
                    $artifacts_update->artifact = $artifact = $Artifacts->newEmptyEntity();
                }

                if ($artifacts_update->apply($artifact)) {
                    $artifacts_update->publication_error = '';
                    if ((!$artifact->has('artifacts_shadow') || $ArtifactsShadow->saveMany($artifact->artifacts_shadow)) &&
                        $Artifacts->save($artifact) &&
                        $ArtifactsUpdates->save($artifacts_update)) {
                        continue;
                    }
                } else {
                    // Attempt to save errors
                    $errors = $artifacts_update->getErrors();
                    $artifacts_update->publication_error = json_encode($errors);

                    foreach (array_keys($errors) as $field) {
                        $artifacts_update->setError($field, [], true);
                    }

                    $ArtifactsUpdates->save($artifacts_update, ['associated' => false]);
                }

                $this->setError('artifacts_updates', __('Failed to update artifact #{0}', $artifacts_update->artifact_id), false);
            }
        } else {
            TableRegistry::get($this->getSource())->loadInto($this, ['Inscriptions.Artifacts.Inscriptions']);
            $Inscriptions = TabelRegistry::get('Inscriptions');

            foreach ($this->inscriptions as $inscription) {
                $inscription->artifact->inscription->is_latest = false;
                if ($Inscriptions->save($inscription->artifact->inscription)) {
                    $inscription->is_latest = true;
                    if ($Inscriptions->save($inscription)) {
                        continue;
                    } else {
                        // If update fails, try to restore the previous version
                        $inscription->artifact->inscription->is_latest = true;
                        $Inscriptions->save($inscription->artifact->inscription);
                    }
                }

                $this->setError('inscriptions', __('Failed to update inscription #{0}', $inscription->id), false);
            }
        }

        return !$this->hasErrors();
    }

    public function isCreatedBy(User $user)
    {
        if (empty($user) || !$user->has('author_id')) {
            return false;
        }
        return $this->created_by == $user->author_id;
    }
}
