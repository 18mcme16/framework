<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dynasty Entity
 *
 * @property int $id
 * @property string|null $polity
 * @property string|null $dynasty
 * @property int|null $sequence
 * @property int|null $provenience_id
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Date[] $dates
 * @property \App\Model\Entity\Ruler[] $rulers
 */
class Dynasty extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'polity' => true,
        'dynasty' => true,
        'sequence' => true,
        'provenience_id' => true,
        'provenience' => true,
        'dates' => true,
        'rulers' => true
    ];

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E4_Period',
            'crm:P1_is_identified_by' => [
                '@type' => 'crm:E41_Appellation',
                'rdfs:label' => $this->dynasty
            ],
            'crm:P7_took_place_at' => self::getEntity($this->provenience),
            'crm:P10_falls_within' => [
                '@type' => 'crm:E4_Period',
                'crm:P1_is_identified_by' => [
                    '@type' => 'crm:E41_Appellation',
                    'rdfs:label' => $this->polity
                ]
            ],
            'crm:P10i_contains' => self::getEntities($this->dates)
        ];
    }
}
