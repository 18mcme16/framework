<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Staff Controller
 *
 * @property \App\Model\Table\StaffTable $Staff
 *
 * @method \App\Model\Entity\Staff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StaffController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view', 'image']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Authors', 'StaffTypes']
        ];
        $staff = $this->paginate($this->Staff);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('staff', 'access_granted'));
    }

    public function image($id)
    {
        $file = glob("./webroot/staff-img/$id*");
        $extension = substr(strrchr($file[0], '.'), 1);
        return $this->getResponse()->withFile("webroot/staff-img/$id.".$extension);
    }
}
