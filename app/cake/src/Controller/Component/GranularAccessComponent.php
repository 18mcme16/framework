<?php
namespace App\Controller\Component;

use App\Model\Entity\Artifact;
use App\Model\Entity\UpdateEvent;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;

class GranularAccessComponent extends Component
{
    public $components = ['GeneralFunctions', 'Auth', 'Paginator'];

    public function initialize(array $config): void
    {
        $this->Artifacts = TableRegistry::get('Artifacts');
        $this->UpdateEvents = TableRegistry::get('UpdateEvents');
        $this->Users = TableRegistry::get('Users');
    }

    /**
     * Load inscriptions and related artifacts when allowed.
     *
     * @param \App\Model\Entity\Artifact $artifact
     */
    public function amendArtifact($artifact)
    {
        // Load related artifacts (composite/seal and the reverse)
        if ($this->canViewPrivateArtifact()) {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses',
                'Impressions',
                'Composites',
                'Seals'
            ]);
        } else {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses' => ['conditions' => ['Witnesses.is_public' => true]],
                'Impressions' => ['conditions' => ['Impressions.is_public' => true]],
                'Composites' => ['conditions' => ['Composites.is_public' => true]],
                'Seals' => ['conditions' => ['Seals.is_public' => true]]
            ]);
        }

        // Load related inscriptions
        if ($artifact->is_atf_public || $this->canViewPrivateInscriptions()) {
            $this->Artifacts->loadInto($artifact, ['Inscriptions']);
        }

        if (is_array($artifact->witnesses)) {
            foreach ($artifact->witnesses as $witness) {
                if ($witness->is_atf_public || $this->canViewPrivateInscriptions()) {
                    $this->Artifacts->loadInto($witness, ['Inscriptions']);
                }
            }
        }
    }

    /**
     * Load artifact updates and inscriptions when allowed.
     *
     * @param \App\Model\Entity\UpdateEvent $event
     */
    public function amendUpdateEvent($event)
    {
        if ($event->update_type == 'artifact') {
            $table = $this->UpdateEvents->ArtifactsUpdates;
            $conditions = ['update_events_id' => $event->id];
        } else {
            $table = $this->UpdateEvents->Inscriptions;
            $conditions = ['update_event_id' => $event->id];
        }

        if (!$this->canViewPrivateArtifact()) {
            $conditions['Artifacts.is_public'] = true;
        }
        if (!$this->canViewPrivateInscriptions() && $table == $this->UpdateEvents->Inscriptions) {
            $conditions['Artifacts.is_atf_public'] = true;
        }

        $results = $this->Paginator->paginate($table->find()->where($conditions)->contain(['Artifacts']));

        if ($event->update_type == 'artifact') {
            $this->UpdateEvents->ArtifactsUpdates->loadInto($results, [
                'Artifacts' => [
                    'Archives',
                    'ArtifactsShadow',
                    'ArtifactTypes',
                    'Collections',
                    'Composites',
                    'ExternalResources',
                    'Genres',
                    'Languages',
                    'Materials',
                    'Origins',
                    'Periods',
                    'Proveniences',
                    'Publications',
                    'Seals'
                ]
            ]);
            $this->amendArtifactsUpdate($results);
            $event->artifacts_updates = $results;
        } else {
            $event->inscriptions = $results;
        }
    }

    /**
     * Censor private fields.
     *
     * @param \App\Model\Entity\ArtifactsUpdate|\App\Model\Entity\ArtifactsUpdate[] $update
     */
    public function amendArtifactsUpdate($update)
    {
        if ($this->isAdmin()) {
            return;
        }

        if (is_array($update) || $update instanceof ResultSet) {
            foreach ($update as $single_update) {
                $this->amendArtifactsUpdate($single_update);
            }
            return;
        }

        foreach (array_keys(Artifact::$privateFlatFields) as $key) {
            $update->unset($key);
        }
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1]);
    }

    /**
     * @return bool
     */
    public function canSubmitEdits()
    {
        if (empty($this->Auth->user())) {
            return false;
        }

        // TODO BEGIN
        // remove to enable crowdsourcing functionality
        // currently restricted to admins and editors
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2])) {
            return false;
        }
        // TODO END

        return $this->Users->get($this->Auth->user('id'))->has('author_id');
    }

    /**
     * @return bool
     */
    public function canAccessEdits(UpdateEvent $event)
    {
        if ($event->status == 'approved' || $event->status == 'submitted') {
            return true;
        }

        if (empty($this->Auth->user())) {
            return false;
        }

        return $this->canReviewEdits() || $event->isCreatedBy($this->Users->get($this->Auth->user('id')));
    }

    /**
     * @return bool
     */
    public function canReviewEdits()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 2]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateArtifact()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 4]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateImage()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 7]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateInscriptions()
    {
        return $this->GeneralFunctions->checkIfRolesExists([1, 5]);
    }
}
