<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Submissions Controller
 *
 * @property \App\Model\Table\SubmissionsTable $Submissions
 * @method \App\Model\Entity\Submission[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubmissionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sections'],
        ];
        $submissions = $this->paginate($this->Submissions);

        $this->set(compact('submissions'));
    }

    /**
     * View method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $submission = $this->Submissions->get($id, [
            'contain' => ['Sections',
                          'ReviewAssignments',
                          'ReviewAssignments.UserSettings',
                          'ReviewAssignments.SubmissionComments',
                          'Queries',
                          'Queries.Notes',
                          'Queries.Notes.UserSettings'],
        ]);

        $this->set(compact('submission'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $submission = $this->Submissions->newEmptyEntity();
        if ($this->request->is('post')) {
            $submission = $this->Submissions->patchEntity($submission, $this->request->getData());
            if ($this->Submissions->save($submission)) {
                $this->Flash->success(__('The submission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submission could not be saved. Please, try again.'));
        }
        $contexts = $this->Submissions->Contexts->find('list', ['limit' => 200]);
        $sections = $this->Submissions->Sections->find('list', ['limit' => 200]);
        $currentPublications = $this->Submissions->CurrentPublications->find('list', ['limit' => 200]);
        $stages = $this->Submissions->Stages->find('list', ['limit' => 200]);
        $this->set(compact('submission', 'contexts', 'sections', 'currentPublications', 'stages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $submission = $this->Submissions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submission = $this->Submissions->patchEntity($submission, $this->request->getData());
            if ($this->Submissions->save($submission)) {
                $this->Flash->success(__('The submission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submission could not be saved. Please, try again.'));
        }
        $contexts = $this->Submissions->Contexts->find('list', ['limit' => 200]);
        $sections = $this->Submissions->Sections->find('list', ['limit' => 200]);
        $currentPublications = $this->Submissions->CurrentPublications->find('list', ['limit' => 200]);
        $stages = $this->Submissions->Stages->find('list', ['limit' => 200]);
        $this->set(compact('submission', 'contexts', 'sections', 'currentPublications', 'stages'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Submission id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submission = $this->Submissions->get($id);
        if ($this->Submissions->delete($submission)) {
            $this->Flash->success(__('The submission has been deleted.'));
        } else {
            $this->Flash->error(__('The submission could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
