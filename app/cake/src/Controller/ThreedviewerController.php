<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

class ThreedviewerController extends AppController
{
    /**
    * intialize method
    *
    * @return \Cake\Http\Response|void
    */
    public function initialize(): void
    {
        parent::initialize();
        $this->url = Router::url("/", true);
        // Set access for public.
        $this->Auth->allow(['index']);
    }
    /**
    * Index method
    *
    * @return \Cake\Http\Response|void
    */
    public function index($id)
    {
        //echo $this->url;die;
        //$this->set("title","3D Model Viewer");
        $this->set("CDLI_NO", "$id");
    }
}
