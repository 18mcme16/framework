<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Languages Controller
 *
 * @property \App\Model\Table\LanguagesTable $Languages
 *
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LanguagesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentLanguages', 'ChildLanguages']
        ];
        $languages = $this->paginate($this->Languages);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('languages', 'access_granted'));
        $this->set('_serialize', 'languages');
    }

    /**
     * View method
     *
     * @param string|null $id Language id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $language = $this->Languages->get($id, [
            'contain' => ['ParentLanguages', 'ChildLanguages']
        ]);

        //$this->set('language', $language);

        $artifacts = TableRegistry::get('ArtifactsGenres');
        $count = $artifacts->find('list', ['conditions' => ['genre_id' => $id]])->count();
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('language', 'count', 'access_granted'));
        $this->set('_serialize', 'language');
    }
}
