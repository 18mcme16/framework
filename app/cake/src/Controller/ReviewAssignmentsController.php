<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ReviewAssignments Controller
 *
 * @property \App\Model\Table\ReviewAssignmentsTable $ReviewAssignments
 * @method \App\Model\Entity\ReviewAssignment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReviewAssignmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Submissions', 'ReviewRounds'],
        ];
        $reviewAssignments = $this->paginate($this->ReviewAssignments);

        $this->set(compact('reviewAssignments'));
    }

    /**
     * View method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reviewAssignment = $this->ReviewAssignments->get($id, [
            'contain' => ['Submissions', 'ReviewRounds','UsersOjs','SubmissionComments'],
        ]);

        $this->set(compact('reviewAssignment'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reviewAssignment = $this->ReviewAssignments->newEmptyEntity();
        if ($this->request->is('post')) {
            $reviewAssignment = $this->ReviewAssignments->patchEntity($reviewAssignment, $this->request->getData());
            if ($this->ReviewAssignments->save($reviewAssignment)) {
                $this->Flash->success(__('The review assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review assignment could not be saved. Please, try again.'));
        }
        $submissions = $this->ReviewAssignments->Submissions->find('list', ['limit' => 200]);
        $reviewRounds = $this->ReviewAssignments->ReviewRounds->find('list', ['limit' => 200]);
        $this->set(compact('reviewAssignment', 'submissions', 'reviewRounds'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reviewAssignment = $this->ReviewAssignments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reviewAssignment = $this->ReviewAssignments->patchEntity($reviewAssignment, $this->request->getData());
            if ($this->ReviewAssignments->save($reviewAssignment)) {
                $this->Flash->success(__('The review assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The review assignment could not be saved. Please, try again.'));
        }
        $submissions = $this->ReviewAssignments->Submissions->find('list', ['limit' => 200]);
        $reviewRounds = $this->ReviewAssignments->ReviewRounds->find('list', ['limit' => 200]);
        $this->set(compact('reviewAssignment', 'submissions', 'reviewRounds'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Review Assignment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reviewAssignment = $this->ReviewAssignments->get($id);
        if ($this->ReviewAssignments->delete($reviewAssignment)) {
            $this->Flash->success(__('The review assignment has been deleted.'));
        } else {
            $this->Flash->error(__('The review assignment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
