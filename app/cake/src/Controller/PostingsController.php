<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 *
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');

        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['news', 'highlights', 'view', 'index']);
    }

    /**
     * News method
     *
     * @return \Cake\Http\Response|void
     */
    public function news()
    {
        $this->paginate = [
            'contain' => ['PostingTypes', 'Artifacts']
        ];
        $postings = $this->paginate($this->Postings);

        $this->set(compact('postings'));
    }

    
    // /**
    //  * Index method
    //  *
    //  * @return \Cake\Http\Response|void
    //  */
    // public function index()
    // {
    //     $this->paginate = [
    //         'limit' => 20,
    //         'contain' => ['Creators', 'Modifiers','PostingTypes']
    //     ];

    //     $postings = $this->paginate($this->Postings);
    //     $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
    //     $this->set(compact('postings', 'access_granted'));
    // }

    /**
     * Highlights method
     *
     * @return \Cake\Http\Response|void
     */
    public function highlights()
    {
        $this->paginate = [
            'contain' => ['PostingTypes', 'Artifacts']
        ];
        $postings = $this->paginate($this->Postings);

        $this->set(compact('postings'));
    }

    /**
     * View method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $posting = $this->Postings->get($id, [
            'contain' => ['PostingTypes', 'Artifacts', 'Creators', 'Modifiers']
        ]);

        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set('access_granted', $access_granted);
        $this->set('posting', $posting);
    }
}
