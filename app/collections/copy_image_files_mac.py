#!/usr/bin/env python3

"Copy files from source to destination, archive them, send notification email."

import argparse
import base64
import codecs
import json
import logging
import os
import shutil
import smtplib
import sys
import urllib.request
import zipfile


image_file_suffix = ['.tif', '_d.tif', '_e.tif', '_ed.tif', '_s.tif', '_l.tif',
                     '_ld.tif', '_ls.tif']


EXTRACTED_ARCHIVE_SIZE_LIMIT = 53687091200 # 50GB


def copy_resources(src, dst_subpath, requested_names):
  '''Copy files requested files from source to destination.

  global:
    current_d_id: int - directory id of current destination directory
    d_id_size: [int] - size of directory at directory id index.
    found_pids: set([str]) - stores all pids that have found relevant files.

  Args:
    src: str - path to the source directory.
    dst_subpath: str - subpath to the destination directory.
    requested_names: set([str]) - the set of requested names.
  '''
  global current_d_id
  global d_id_size
  global found_pids

  file_count = 0
  files_to_copy = []

  # filenames = os.listdir(src)
  # files_to_copy = requested_names & set(filenames)
  for filenames in requested_names:
    if os.path.isfile(src+"/"+filenames):
      files_to_copy.append(filenames)
  dst = tmp_directory_base_ids[current_d_id] + dst_subpath

  for filename in files_to_copy:
    found_pids.add(filename[0:7])

  for filename in files_to_copy:
    # Create new destination directories and update relevant tracking variables
    if d_id_size[current_d_id] > EXTRACTED_ARCHIVE_SIZE_LIMIT:
      current_d_id += 1
      d_id_size.append(0)
      create_dst_directory(current_d_id)
      dst = tmp_directory_base_ids[current_d_id] + dst_subpath

    srcname = os.path.join(src, filename)
    dstname = os.path.join(dst, filename)

    logging.info("Copying %s to %s." % (srcname, dstname))
    shutil.copy(srcname, dstname)
    d_id_size[current_d_id] += os.path.getsize(dstname)
    file_count += 1

  if file_count > 0:
    logging.info("Copied %d files from %s" % (file_count, src))


def save_metadata(pids, found_pids, filename):
  '''Save a table of contents with metadata information of files copied.

  Args:
    pids: {str: str} - pid to metadata mapping.
    found_pids: set([str]) - set of pids which had files copied.
    filename: file to store table of contents.
  '''
  with codecs.open(filename, 'w', 'utf-8') as output:
    if found_pids:
      header = "CDLI no.\tMuseum no.\tAccession no.\tExcavation no.\t" \
               "Period\tProvenience\tPrimary publication\n"
      output.write(header)

    for pid in sorted(found_pids):
      output.write(pids.get(pid, '') + '\n')


def archive(src_directory, archive_base_name):
  '''Archive the files in the source directory to the given location.

  Args:
    src_directory: str - path to the source directory.
    archive_base_name: str - path to the archive (no extension).
  '''
  make_archive(archive_base_name, "zip", src_directory,
               verbose=True, logger=logging)



###
### Helper functions
###

def create_dst_directory(d_id):
  '''Create destination directories/subdirectories with given directory id.

  global:
    directory_sub_pix: [str] - directory branch based on pixels.
    tmp_directory_base_ids: [str] - directory id mapped to directory path.
    tmp_directory_sub: str - directory branch without _l.
    tmp_directory_sub_l: str - directory branch with _l.
    user: str - username of requester.
    token: str - unique token to distinguish this request.

  Args:
    d_id: int - destination directory id to create directories for.
  '''
  global directory_sub_pix
  global tmp_directory_base_ids
  global tmp_directory_sub
  global tmp_directory_sub_l
  global user
  global token

  # Base + d_id
  tmp_directory_base = (
      "/mnt/cdli_collections_new/collection_downloads/%s/tmp_%s" % \
      (user, token))
  # tmp_directory_base = (
  #     "/Volumes/cdli_collections_new/collection_downloads/%s/tmp_%s" % \
  #     (user, token))
  tmp_directory_base_ids.append("%s_%03d" % (tmp_directory_base, d_id))

  # Base + d_id + without _l branch + pixel branch
  for sub in directory_sub_pix:
    tmp_directory = tmp_directory_base_ids[d_id] + tmp_directory_sub + sub
    if not os.path.exists(tmp_directory):
      os.makedirs(tmp_directory)
    logging.info("Created temporary directory %s." % tmp_directory)

  # Base + d_id + with _l branch
  tmp_directory_l = tmp_directory_base_ids[d_id] + tmp_directory_sub_l
  if not os.path.exists(tmp_directory_l):
    os.makedirs(tmp_directory_l)
  logging.info("Created temporary directory %s." % tmp_directory_l)


def _make_zipfile(base_name, base_dir, verbose=0, dry_run=0, logger=None):
  """Create a zip file from all the files under 'base_dir'.

  The output zip file will be named 'base_dir' + ".zip".  Uses the
  "zipfile" Python module. Returns the name of the output zip
  file.
  """
  zip_filename = base_name + ".zip"
  archive_dir = os.path.dirname(base_name)

  if not os.path.exists(archive_dir):
    if logger is not None:
      logger.info("creating %s", archive_dir)
    if not dry_run:
      os.makedirs(archive_dir)

  if logger is not None:
    logger.info("creating '%s' and adding '%s' to it",
                zip_filename, base_dir)

  if not dry_run:
    zip = zipfile.ZipFile(zip_filename, "w",
                          compression=zipfile.ZIP_DEFLATED,
                          allowZip64=True)

    for dirpath, dirnames, filenames in os.walk(base_dir):
      for name in filenames:
        path = os.path.normpath(os.path.join(dirpath, name))
        if os.path.isfile(path):
          zip.write(path, path)
          if logger is not None:
            logger.info("adding '%s'", path)
    zip.close()

  return zip_filename


_ARCHIVE_FORMATS = {
  'zip':   (_make_zipfile, [],"ZIP file")
  }


def make_archive(base_name, format, root_dir=None, base_dir=None, verbose=0,
                 dry_run=0, owner=None, group=None, logger=None):
  """Create an archive file (eg. zip).

  'base_name' is the name of the file to create, minus any format-specific
  extension; 'format' is the archive format: "zip".

  'root_dir' is a directory that will be the root directory of the
  archive; ie. we typically chdir into 'root_dir' before creating the
  archive.  'base_dir' is the directory where we start archiving from;
  ie. 'base_dir' will be the common prefix of all files and
  directories in the archive.  'root_dir' and 'base_dir' both default
  to the current directory.  Returns the name of the archive file.

  'owner' and 'group' are used when creating a tar archive. By default,
  uses the current owner and group.
  """
  save_cwd = os.getcwd()
  if root_dir is not None:
    if logger is not None:
      logger.debug("changing into '%s'", root_dir)
    base_name = os.path.abspath(base_name)
    if not dry_run:
      os.chdir(root_dir)

  if base_dir is None:
    base_dir = os.curdir

  kwargs = {'dry_run': dry_run, 'logger': logger}

  try:
    format_info = _ARCHIVE_FORMATS[format]
  except KeyError:
    raise ValueError( "unknown archive format '%s'" % format)

  func = format_info[0]
  for arg, val in format_info[1]:
    kwargs[arg] = val

  if format != 'zip':
    kwargs['owner'] = owner
    kwargs['group'] = group

  try:
    filename = func(base_name, base_dir, **kwargs)
  finally:
    if root_dir is not None:
      if logger is not None:
        logger.debug("changing back to '%s'", save_cwd)
      os.chdir(save_cwd)

  return filename


##
## Setup
##

def get_entries(filename):
  '''Converts the json encoded dictionary to dictionary.

  Args:
    filename: str - path to the file of pids.

  Returns:
    {str: str} - the dictionary pids that map to metadata.
  '''
  with codecs.open(filename, 'r', 'utf-8') as pid_file:
    pids = json.loads(pid_file.read())

  return pids


def parse_args():
  '''Sets up usage details and help information.

  Returns:
    A object with attributes for each argument.
  '''
  parser = argparse.ArgumentParser(description="Copy files from source to "
                                               "destination and then archive.")

  parser.add_argument('username',
                      help="username of requester")
  parser.add_argument('collection_password',
                      help="password of ftp collection user")
  parser.add_argument('request_token',
                      help="unique token to distinguish this request")

  return parser.parse_args()


args = parse_args()

user = args.username
collection_password = args.collection_password
token = args.request_token

# Path to log file
log_filepath = (
  "/mnt/cdli_collections_new/collection_downloads/%s/pids_%s.log" % \
  (user, token))

logging.basicConfig(
  filename=log_filepath,
  filemode='w',
  format=('%(asctime)s %(name)s '
          '%(levelname)s %(message)s'),
          datefmt='%m/%d/%Y %H:%M:%S',
          level=logging.INFO)

# Log arguments but hide password
args.collection_password = 'hidden'
logging.info(args)
args.collection_password = collection_password

found_pids = set()            # set of pids of files that were copied.
current_d_id = 0              # destination directory id.
d_id_size = [0]               # size of directory at directory id index.
tmp_directory_base_ids = []   # base paths of destination directories
                              # at directory id index.
archive_names = []            # list of the names of the archives created
                              # indexed by directory id

# Path to file with the p_ids (json encoded)
p_ids_filepath = (
  "/mnt/cdli_collections_new/collection_downloads/%s/pids_%s.txt" % \
  (user, token))

# Source directory of images
src_directory = "/mnt/cdli_archivalfiles_new/imagefiles/tif4jpg"
src_directory_l = "/mnt/cdli_archivalfiles_new/imagefiles/tif4jpg_l"

# Destination subpaths
tmp_directory_sub = "/%s_imagefiles_cdli/tif4jpg" % user
tmp_directory_sub_l = "/%s_imagefiles_cdli/tif4jpg_l" % user
directory_sub_pix = ['/075ppi', '/150ppi', '/300ppi', '/600ppi']
                     #'/nimrud/nimrud075', '/nimrud/nimrud150',
                     #'/nimrud/nimrud300', '/nimrud/nimrud600']

# Path to the archive file to be created, excluding the extension.
# For example: /files/archive to create an archive with all the images at
# /files/archive.zip
archive_base_name = (
  "/mnt/cdli_collections_new/collection_downloads/%s/%s_%s" % \
  (user, user, token))

# Load the pid data from file
p_ids = get_entries(p_ids_filepath)

all_filenames = set()
for key in p_ids.keys():
  all_filenames |= set(map(lambda suffix: key + suffix, image_file_suffix))

try:
  logging.info("Creating initial temporary directories...")
  create_dst_directory(current_d_id)
  logging.info("Creating initial temporary directories...completed.")

  logging.info("Copying files...")
  for sub in directory_sub_pix:
    copy_resources(src_directory + sub, tmp_directory_sub + sub,
                   all_filenames)
  copy_resources(src_directory_l, tmp_directory_sub_l, all_filenames)
  logging.info("Copying files...completed.")

  # logging.info("Writing metadata text...")
  # save_metadata(p_ids, found_pids,
  #               tmp_directory_base_ids[0] + '/contents.txt')
  # logging.info("Writing metadata text...completed.")

  logging.info("Archiving...")
  for d_id in range(current_d_id + 1):
    archive_base_name_id = archive_base_name + "_%03d" % d_id
    archive(tmp_directory_base_ids[d_id], archive_base_name_id)
    archive_names.append(archive_base_name_id + '.zip')
  logging.info("Archiving...completed.")

except Exception as e:
  logging.error(e)
finally:
  for d_id in range(current_d_id + 1):
    logging.info("Deleting temporary directory %s..." % \
                 tmp_directory_base_ids[d_id])
    shutil.rmtree(tmp_directory_base_ids[d_id])
    logging.info("Deleted temporary directory %s." % \
                 tmp_directory_base_ids[d_id])
def main():
  global archive_names
  print (archive_names[-1])

if __name__ == '__main__':
  main()